const configs = {
  API_DOMAIN: process.env.NEXT_PUBLIC_API_DOMAIN,
  APP_ENV: process.env.NEXT_PUBLIC_APP_ENV,
};

export default configs;
