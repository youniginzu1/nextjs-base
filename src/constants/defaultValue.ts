export const MIN_LENGTH_PASSWORD = 6;
export const MAX_LENGTH_PASSWORD = 25;
export const MAX_LENGTH_USERNAME = 50;
export const EXPIRES_COOKIE = 1;

export const defaultFilterEx = {
  pageIndex: 1,
  pageSize: 10,
};
