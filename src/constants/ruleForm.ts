import { i18n } from 'next-i18next';

import { MIN_LENGTH_PASSWORD } from './defaultValue';
import {
  REGEX_EMAIL,
  REGEX_EMOJI,
  REGEX_JAPANESE,
  REGEX_PASSWORD,
  REGEX_PHONE,
} from './regex';

export const RuleForm = {
  password: () => [
    () => ({
      validator(_: any, value: any) {
        const stringValue = value?.toString();
        const stringLength = stringValue?.length;
        if (stringLength === 0) {
          return Promise.reject(
            new Error(i18n?.t('validate.passwordRequired') as string)
          );
        }
        if (!stringValue?.trim()) {
          return Promise.reject(
            new Error(i18n?.t('validate.passwordNoAllSpaces') as string)
          );
        }
        if (
          stringValue.match(REGEX_EMOJI) ||
          stringValue.match(REGEX_JAPANESE)
        ) {
          return Promise.reject(
            new Error(i18n?.t('validate.passwordWrongFormat') as string)
          );
        }
        if (stringLength < MIN_LENGTH_PASSWORD) {
          return Promise.reject(
            new Error(i18n?.t('validate.passwordTooShort') as string)
          );
        }
        return Promise.resolve();
      },
    }),
  ],
  newPassword: () => [
    () => ({
      validator(_: any, value: any) {
        const stringValue = value?.toString();
        const stringLength = stringValue?.length;
        if (stringLength === 0) {
          return Promise.reject(
            new Error(i18n?.t('validate.passwordRequired') as string)
          );
        }
        if (!stringValue?.trim()) {
          return Promise.reject(
            new Error(i18n?.t('validate.passwordNoAllSpaces') as string)
          );
        }
        if (
          stringValue.match(REGEX_EMOJI) ||
          stringValue.match(REGEX_JAPANESE)
        ) {
          return Promise.reject(
            new Error(i18n?.t('validate.passwordWrongFormat') as string)
          );
        }
        if (stringLength < MIN_LENGTH_PASSWORD) {
          return Promise.reject(
            new Error(i18n?.t('validate.passwordTooShort') as string)
          );
        }
        return Promise.resolve();
      },
    }),
  ],
  confirmPassword: () => [
    ({ getFieldValue }: any) => ({
      validator(_: any, value: any) {
        if (!value || getFieldValue('new_password') === value) {
          return Promise.resolve();
        }
        const stringValue = value?.toString();
        const stringLength = stringValue?.length;
        if (stringLength === 0) {
          return Promise.reject(
            new Error(i18n?.t('validate.passwordRequired') as string)
          );
        }
        if (!stringValue?.trim()) {
          return Promise.reject(
            new Error(i18n?.t('validate.passwordNoAllSpaces') as string)
          );
        }
        if (
          stringValue.match(REGEX_EMOJI) ||
          stringValue.match(REGEX_JAPANESE)
        ) {
          return Promise.reject(
            new Error(i18n?.t('validate.passwordWrongFormat') as string)
          );
        }
        if (stringLength < MIN_LENGTH_PASSWORD) {
          return Promise.reject(
            new Error(i18n?.t('validate.passwordTooShort') as string)
          );
        }
        return Promise.reject(
          new Error(i18n?.t('validate.passwordNotMatch') as string)
        );
      },
    }),
  ],
  username: () => [
    () => ({
      validator(_: any, value: any) {
        const stringValue = value?.toString();
        const stringLength = stringValue?.length;
        if (stringLength === 0) {
          return Promise.reject(
            new Error(i18n?.t('validate.usernameRequired') as string)
          );
        }
        if (!stringValue?.trim()) {
          return Promise.reject(
            new Error(i18n?.t('validate.usernameNoAllSpaces') as string)
          );
        }
        if (
          stringValue?.[0] === ' ' ||
          stringValue?.[stringLength - 1] === ' ' ||
          !stringValue?.match(REGEX_EMAIL)
        ) {
          return Promise.reject(
            new Error(i18n?.t('validate.usernameWrongFormat') as string)
          );
        }
        return Promise.resolve();
      },
    }),
  ],
};

export const commonValidate = {
  email: {
    pattern: REGEX_EMAIL,
    message: i18n?.t('validate.emailWrongFormat'),
  },
  phone: {
    pattern: REGEX_PHONE,
    message: i18n?.t('validate.phoneIsNotValid'),
  },
  required: {
    required: true,
    message: i18n?.t('validate.fieldIsRequired'),
  },
  password: {
    pattern: REGEX_PASSWORD,
    message: i18n?.t('validate.passwordWrongFormat'),
  },
  whiteSpace: {
    whitespace: true,
    message: i18n?.t('validate.noAllSpaces'),
  },
};
