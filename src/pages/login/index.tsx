import { Button, Card, Checkbox, Form, Input, Row } from 'antd';
import { RuleForm } from 'constants/ruleForm';
import { useRouter } from 'next/router';
import { useTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

import styles from 'styles/pages/Login.module.scss';
import { useAuth } from 'utils/helper/authentication';

export default function Login() {
  const [t] = useTranslation();
  const router = useRouter();

  const { login, loadingLogin } = useAuth();

  const handleSubmit = (payload: any) => {
    if (!loadingLogin) {
      login(payload);
    }
  };

  return (
    <div className={styles.login}>
      <Card bordered className={styles.form}>
        <Form onFinish={handleSubmit}>
          <Row justify="center">
            <h2>{t('login.title')}</h2>
          </Row>
          <Form.Item
            label={t('formLabel.email')}
            name="email"
            rules={RuleForm.username()}
            labelAlign="left"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
          >
            <Input placeholder={t('placeholder.email') || ''} />
          </Form.Item>
          <Form.Item
            label={t('formLabel.password')}
            name="password"
            rules={RuleForm.password()}
            labelAlign="left"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
          >
            <Input.Password placeholder={t('placeholder.password') || ''} />
          </Form.Item>
          <Form.Item name="rememberMe" valuePropName="checked">
            <Checkbox> {t('formLabel.rememberMe')}</Checkbox>
          </Form.Item>
          <Form.Item labelCol={{ span: 24 }}>
            <Button block type="primary" htmlType="submit">
              {t('common.login').toUpperCase()}
            </Button>
          </Form.Item>
          <Form.Item labelCol={{ span: 24 }}>
            <Button
              block
              type="dashed"
              htmlType="button"
              onClick={() => router.push('/sign-up')}
            >
              {t('common.signUp').toUpperCase()}
            </Button>
          </Form.Item>
          <div>
            <p>Account: admin / 123456</p>
          </div>
        </Form>
      </Card>
    </div>
  );
}

export const getStaticProps = async ({ locale }: any) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common'])),
    },
  };
};
