import { Button, Card, Form, Input, Row } from 'antd';
import { useRouter } from 'next/router';
import { useTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

import styles from 'styles/pages/SignUp.module.scss';

export default function SignUp() {
  const [t] = useTranslation();
  const router = useRouter();

  const handleSubmit = () => {};

  return (
    <div className={styles.signUp}>
      <Card bordered className={styles.form}>
        <Form onFinish={handleSubmit}>
          <Row justify="center">
            <h2>{t('signUp.title')}</h2>
          </Row>
          <Form.Item
            label={t('formLabel.email')}
            name="email"
            rules={[
              {
                required: true,
                message: t('validate.emailRequired') || '',
              },
            ]}
            labelAlign="left"
            labelCol={{ span: 24 }}
            wrapperCol={{ span: 24 }}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label={t('formLabel.password')}
            name="password"
            rules={[
              {
                required: true,
                message: t('validate.passwordRequired') || '',
              },
            ]}
            labelAlign="left"
            labelCol={{ span: 24 }}
            wrapperCol={{ span: 24 }}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            label={t('formLabel.confirmPassword')}
            name="passwordConfirm"
            rules={[
              {
                required: true,
                message: t('validate.passwordRequired') || '',
              },
            ]}
            dependencies={['password']}
            labelAlign="left"
            labelCol={{ span: 24 }}
            wrapperCol={{ span: 24 }}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item labelCol={{ span: 24 }}>
            <Button block type="primary" htmlType="submit">
              {t('common.signUp')}
            </Button>
          </Form.Item>
          <Form.Item labelCol={{ span: 24 }}>
            <Button
              block
              type="dashed"
              htmlType="button"
              onClick={() => router.push('/login')}
            >
              {t('common.login')}
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </div>
  );
}

export const getStaticProps = async ({ locale }: any) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common'])),
    },
  };
};
