import { useMutation } from '@tanstack/react-query';
import { useRouter } from 'next/router';

import { postLogin } from 'api/auth';
import storage from './storage';
import { CookieKey } from 'constants/enum';

export const useAuth = () => {
  const router = useRouter();

  const { mutate: login, isLoading: loadingLogin } = useMutation(
    (payload: any) => postLogin(payload),
    {
      onSuccess: (data: any) => {
        const token = data?.token;
        const refreshToken = data?.refreshToken;
        if (token && refreshToken) {
          storage.setItem(CookieKey.TOKEN, token);
          storage.setItem(CookieKey.REFRESH_TOKEN, refreshToken);
        }
        router.push('/');
      },
    }
  );

  const logout = () => {
    storage.removeItem(CookieKey.TOKEN);
    storage.removeItem(CookieKey.REFRESH_TOKEN);
    router.push('/login');
  };

  return { login, loadingLogin, logout };
};
