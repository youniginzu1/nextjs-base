import { message } from 'antd';
import moment from 'moment';
import { i18n } from 'next-i18next';

import configs from 'constants/config';
import { FormatTime } from 'constants/enum';

//Handle error
export const handleErrorMessage = (error: any) => {
  message.destroy();
  message.error(getErrorMessage(error));
  if (configs.APP_ENV !== 'prod') {
    console.log(error);
  }
};

export const getErrorMessage = (error: any) => {
  return error?.response?.data?.errorMessage || i18n?.t('common.error');
};

//Base 64
export const getBase64 = (img: File): Promise<string> =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => resolve(reader.result as string));
    reader.readAsDataURL(img);
  });

//Table
export const getIndexTable = (
  pageIndex: number,
  pageSize: number,
  current: number
) => {
  return (pageIndex - 1) * pageSize + current + 1;
};

//Date time
export const convertTimeToLocal = (time?: string, format?: string) => {
  if (!time) return '';
  return moment(time)
    .format(format || FormatTime.DATE_TIME)
    .toString();
};

//Format text
export const trimText = (str?: string) => {
  if (!str) return '';
  return str.trim().replace(/\s+/gm, ' ');
};
