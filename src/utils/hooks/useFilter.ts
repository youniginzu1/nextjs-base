import { useState } from 'react';
import { useDebouncedCallback } from 'use-debounce';

import { IFilter } from 'constants/interface';
import { DebounceTime } from 'constants/enum';
import { defaultFilterEx } from 'constants/defaultValue';

export default function useFilter(defaultFilter?: IFilter) {
  const [filter, setFilter] = useState<IFilter>(
    defaultFilter ?? defaultFilterEx
  );

  const handleFilterChange = (changeValue: IFilter) => {
    setFilter({
      ...filter,
      ...changeValue,
      pageIndex: 1,
    });
  };

  const debounceKeyword = useDebouncedCallback((keyword) => {
    handleFilterChange({ keyword });
  }, DebounceTime.DEFAULT);

  const keywordSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    debounceKeyword(e.target.value);
  };
  const handlePageChange = (page: number, pageSize: number) => {
    setFilter({ ...filter, pageIndex: page, pageSize });
  };
  const resetFilter = () => {
    if (defaultFilter) {
      setFilter({ ...defaultFilter });
    } else {
      setFilter({ ...defaultFilterEx });
    }
  };

  return {
    filter,
    handleFilterChange,
    handlePageChange,
    resetFilter,
    handleSearch: {
      keywordSearch,
    },
  };
}
